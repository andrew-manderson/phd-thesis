RSCRIPT = Rscript
PLOT_SETTINGS = scripts/common/setup-ggplot-theme.R
#TEX_FILES = $(wildcard tex-input/*.tex) \
#	$(wildcard tex-input/*/*.tex) \
#	$(wildcard tex-input/*/*/*.tex)

# useful compound make components
PLOTS = plots
RDS = rds
SCRIPTS = scripts

# if you wildcard the all-target, then nothing will happen if the target doesn't
# exist (no target). hard code the target.
# CHANGE THIS:
BASENAME = main
BASEFILE = $(BASENAME).tex
THESIS = $(BASENAME).pdf

all : $(THESIS)

PREAMBLE = tex-input/pre.tex
FRONTMATTER = frontmatter/abstract.tex \
	frontmatter/acknowledgements.tex \
	frontmatter/declaration.tex \
	frontmatter/thesis-info.tex

BIBLIOGRAPHY = bibliography/full-thesis-bib.bib

# ================================ Intro files =================================
INTRO_TOP_FILE = chapters/intro.tex
INTRO_TEX_FILES = $(wildcard tex-input/intro/*.tex) \
	$(wildcard tex-input/intro/*/*.tex)

INTRO_BASENAME = intro
INTRO_PLOTS = $(PLOTS)/$(INTRO_BASENAME)
INTRO_SCRIPTS = $(SCRIPTS)/$(INTRO_BASENAME)
INTRO_ALL_PLOTS = 

INTRO_KDE_PLOT = $(INTRO_PLOTS)/kde-example.pdf
$(INTRO_KDE_PLOT) : \
	$(INTRO_SCRIPTS)/plot-kde-example.R
	$(RSCRIPT) $< \
		--output $@

INTRO_ALL_PLOTS += $(INTRO_KDE_PLOT)

INTRO_GP_PLOT = $(INTRO_PLOTS)/gp-noise-example.pdf
$(INTRO_GP_PLOT) : \
	$(INTRO_SCRIPTS)/plot-gp-approx.R
	$(RSCRIPT) $< \
		--output $@

INTRO_ALL_PLOTS += $(INTRO_GP_PLOT)

INTRO_PARETO_FRONT_PLOT = $(INTRO_PLOTS)/pareto-front-example.pdf
$(INTRO_PARETO_FRONT_PLOT) : \
	$(INTRO_SCRIPTS)/plot-pareto-front.R 
	$(RSCRIPT) $< \
		--output $@

INTRO_ALL_PLOTS += $(INTRO_PARETO_FRONT_PLOT)

INTRO_ALL_FILES = $(INTRO_TOP_FILE) $(INTRO_TEX_FILES) $(INTRO_ALL_PLOTS)

# ============================= WSRE chapter files =============================
WSRE_TOP_FILE = chapters/wsre.tex
WSRE_TEX_FILES = $(wildcard tex-input/wsre/*.tex) \
	$(wildcard tex-input/wsre/*/*.tex)

WSRE_PLOTS = $(wildcard plots/wsre/*/*.p*)
WSRE_ALL_FILES = $(WSRE_TOP_FILE) $(WSRE_TEX_FILES) $(WSRE_PLOTS)

# ============================= MPHI chapter files =============================
MPHI_TOP_FILE = chapters/mphi.tex
MPHI_APPENDIX = appendices/mphi-appendix.tex
MPHI_TEX_FILES = $(wildcard tex-input/MPHI/*.tex) \
	$(wildcard tex-input/mphi/*/*.tex)

MPHI_PLOTS = $(wildcard plots/MPHI/*/*.p*)
MPHI_ALL_FILES = $(MPHI_TOP_FILE) $(MPHI_TEX_FILES) $(MPHI_PLOTS) $(MPHI_APPENDIX)

# ============================= PBBO chapter files =============================
PBBO_TOP_FILE = chapters/pbbo.tex
PBBO_APPENDIX = appendices/pbbo-appendix.tex
PBBO_TEX_FILES = $(wildcard tex-input/pbbo/*.tex) \
	$(wildcard tex-input/pbbo/*/*.tex)

PBBO_PLOTS = $(wildcard plots/pbbo/*/*.p*)
PBBO_ALL_FILES = $(PBBO_TOP_FILE) $(PBBO_TEX_FILES) $(PBBO_PLOTS) $(PBBO_APPENDIX)

# ============================== conclusion files ==============================
CONC_TOP_FILE = chapters/conc.tex
CONC_TEX_FILES = $(wildcard tex-input/conc/*.tex)

CONC_ALL_FILES = $(CONC_TOP_FILE) $(CONC_TEX_FILES)

# ================================= final defs =================================
ALL_MAINMATTER_FILES = $(INTRO_ALL_FILES) \
	$(WSRE_ALL_FILES) \
	$(MPHI_ALL_FILES) \
	$(PBBO_ALL_FILES) \
	$(CONC_ALL_FILES)

clean : 
	latexmk -C; trash main-MAC-ATI0329* main.bbl main.brf main.lot main.loa main.nlo main.lof main.toc; trash chapters/*.aux

$(THESIS) : $(BASEFILE) $(PREAMBLE) $(FRONTMATTER) $(ALL_MAINMATTER_FILES) $(BIBLIOGRAPHY)
	latexmk -xelatex $(BASENAME).tex
