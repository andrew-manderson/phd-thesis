\chapter{Supplementary material to Chapter \ref{chp:mphi}}
\label{mphi-appendix-full}

\hypertarget{log-pooling-gaussian-densities}{%
\section{Log pooling Gaussian
densities}\label{log-pooling-gaussian-densities}}

We can exactly compute \(\pd_{\text{pool}}\) when logarithmically
pooling Gaussian densities. Noting that, in the one dimensional case,
\(\text{N}(\phi; \mu, \sigma^2)^{\lambda_{\modelindex}} = \text{N}(\phi; \mu, \frac{\sigma^2}{\lambda_{\modelindex}})\),
we use the results of \citet{bromiley_products_2003} and write
\input{tex-input/mphi/multiple-phi/0070-log-pooling-gaussian.tex}\noindent
hence
\(\pd_{\text{pool}}(\phi_{1 \cap 2}, \phi_{2 \cap 3}) = \text{N}((\phi_{1 \cap 2}, \phi_{2 \cap 3});\, \mu_{\text{log}}, \, \Sigma_{\text{log}})\).
The choice of \(\lambda_{2}\) is critical; by controlling the
contribution of \(\pd_{2}\) to \(\pd_{\text{pool}}\), \(\lambda_{2}\)
controls the degree of correlation present in the latter. The left hand
column of Figure \ref{fig:pooled_densities_plot} illustrates this
phenomena. When
\(\lambda_{1} = \lambda_{3} = 0 \implies \lambda_{2} = 1\), all
correlation in \(\pd_{2}\) is present in \(\pd_{\text{pool}}\). The
correlation decreases for increasing values of \(\lambda_{1}\) until
\(\lambda_{1} = \lambda_{3} = 0.5 \implies \lambda_{2} = 0\), where no
correlation persists.

\hypertarget{sequential-sampler}{%
\section{Sequential sampler}\label{sequential-sampler}}

\input{tex-input/mphi/multi-stage-sampler/0001-seq-sampler-dag.tex}

Figure \ref{fig:seq-sampler-dag} depicts graphically the strategy
employed by the sequential sampler. The sequential sampler assumes that
the pooled prior decomposes such that
\input{tex-input/mphi/multi-stage-sampler/0002-sequential-sampler-decomposition.tex}\noindent
This is necessary to avoid sampling all components of
\(\symbf{\phi}\) in the first stage. All pooled priors trivially
satisfy \eqref{eqn:sequential-sampler-decomposition}, as we can assume
all but \(\pd_{\text{pool}, 3}(\phi_{1 \cap 2}, \phi_{2 \cap 3})\) are
improper, flat distributions. However, including some portion of the
pooled prior in each stage of the sampler can improve performance, and
eliminate computational instabilities when submodel likelihoods contain
little information.

\hypertarget{stage-one-1}{%
\subsection{Stage one}\label{stage-one-1}}

Stage one of the sequential sampler targets
\input{tex-input/mphi/multi-stage-sampler/0020-stage-one-target.tex}\noindent
using a generic proposal kernel for both \(\phi_{1 \cap 2}\) and
\(\psi_{1}\). The corresponding acceptance probability for a proposed
update from \((\phi_{1 \cap 2}, \psi_{1})\) to
\((\phi_{1 \cap 2}^{*}, \psi_{1}^{*})\) is
\input{tex-input/mphi/multi-stage-sampler/0021-stage-one-acceptance-probability.tex}

\hypertarget{stage-two-1}{%
\subsection{Stage two}\label{stage-two-1}}

The stage two target augments the stage one target by including the
second submodel, corresponding prior marginal distribution, and an
additional pooled prior term
\input{tex-input/mphi/multi-stage-sampler/0030-stage-two-target.tex}\noindent
A Metropolis-within-Gibbs strategy is employed, where the stage one
samples are used as a proposal for \(\phi_{1 \cap 2}\), whilst a generic
proposal kernel is used for \(\psi_{2}\) and \(\phi_{2 \cap 3}\). Thus
the proposal distributions for \(\phi_{1 \cap 2}^{*}\) and
\((\phi_{2 \cap 3}^{*}, \psi_{2}^{*})\) are
\input{tex-input/mphi/multi-stage-sampler/0031-stage-two-gibbs-updates.tex}\noindent
The acceptance probability for this proposal strategy is
\input{tex-input/mphi/multi-stage-sampler/0032-stage-two-acceptance-probabilities.tex}\noindent
Our judicious choice of proposal distribution has resulted in a
cancellation in Equation
\eqref{eqn:stage-two-acceptance-probabilities-one} which removes all
terms related to \(\pd_{1}\). Similarly, all terms related to
\(\pd_{1}\) are constant -- hence cancel -- in Equation
\eqref{eqn:stage-two-acceptance-probabilities-two}. This eliminates any
need to re-evaluate the first submodel.

\hypertarget{stage-three}{%
\subsection{Stage three}\label{stage-three}}

In stage three we target the full melded posterior
\input{tex-input/mphi/multi-stage-sampler/0044-stage-three-target.tex}\noindent
The target has now been broadened to include terms from the third
submodel and the entirety of the pooled prior. Again, we employ a
Metropolis-within-Gibbs sampler, with proposals drawn such that
\input{tex-input/mphi/multi-stage-sampler/0045-stage-three-gibbs-updates.tex}\noindent
which leads to acceptance probabilities of
\input{tex-input/mphi/multi-stage-sampler/0046-stage-three-acceptance-probabilities.tex}\noindent
The informed choice of proposal distribution for
(\(\phi_{1 \cap 2}, \phi_{2 \cap 3}, \psi_{1}, \psi_{2}\)) has allowed
us to target the full melded posterior without needing to evaluate all
submodels simultaneously.

\hypertarget{normal-approximation-calculations}{%
\section{Normal approximation
calculations}\label{normal-approximation-calculations}}

Substituting in the approximations of Section
\ref{normal-approximations-to-submodel-components} to Equation
\eqref{eqn:normal-approx-melded-posterior-target} yields the approximate
melded posterior
\input{tex-input/mphi/multiple-normal-approximation/0020-normal-approximation-approximate-target.tex}\noindent
Noting that the product of independent normal densities is an
unnormalised multivariate normal density with independent components, we
rewrite Equation \eqref{eqn:normal-approximation-approximate-target} as
\input{tex-input/mphi/multiple-normal-approximation/0030-normal-approx-nu-de-form.tex}\noindent
The ratio of normal densities is also an unnormalised normal density,
and hence Equation \eqref{eqn:normal-approx-nu-de-form} simplifies to
\input{tex-input/mphi/multiple-normal-approximation/0041-final-normal-approx-appendix.tex}

\hypertarget{calculating-the-cumulative-fluid-balance-from-the-raw-fluid-data}{%
\section{Calculating the cumulative fluid balance from the raw fluid
data}\label{calculating-the-cumulative-fluid-balance-from-the-raw-fluid-data}}

In the raw fluid data each patient has
\(\tilde{l} = 1, \ldots, \tilde{L}_{i}\) observations. Each observation
\(\tilde{x}_{i, \tilde{l}}\) is typically a small fluid administration
(e.g.~an injection of some medicine in saline solution), or a fluid
discharge (almost always urine excretion). The observations have
corresponding observation times \(\tilde{u}_{i, \tilde{l}}\), with
\(\tilde{\symbf{u}}_{i} = \{\tilde{u}_{i, 1}, \ldots, \tilde{u}_{i, \tilde{L}_{i}}\}\)
and
\(\tilde{\symbf{x}}_{i} = \{\tilde{x}_{i, 1}, \ldots, \tilde{x}_{i, \tilde{L}_{i}}\}\).
We code the fluid administrations/inputs as positive values, and the
excretions/outputs as negative values. Each patient has an enormous
number of raw fluid observations \((L_{i} \ll \tilde{L}_{i})\) and it is
computationally infeasible to consider them all at once. We aggregate
the raw fluid observations into 8-hourly changes in fluid balance. From
these 8-hourly changes we calculate the cumulative fluid balance.

Mathematically, we define an ordered vector of boundary values
\begin{equation}
  \symbf{v}_{i} = (\lfloor \min\{\tilde{\symbf{u}}_{i}\} \rfloor,  \lfloor \min\{\tilde{\symbf{u}}_{i}\} \rfloor + \frac{1}{3}, \ldots, \lceil \max\{\tilde{\symbf{u}}_{i}\} \rceil),
\end{equation} noting that \(\dim(\symbf{v}_{i}) = L_{i} + 1\).
Because the observation times encoded as \emph{days since ICU admission}
and we are interested in the 8-hourly changes, our floor and ceiling
functions round down or up to the appropriate third respectively. The
raw fluid observations are then divided up into \(L_{i}\) subsets of
\(\{\tilde{\symbf{x}}_{i}, \tilde{\symbf{u}}_{i}\}\) based on
which boundary values the observation falls in between: \begin{equation}
  V_{i, l} = \left\{
    \{\tilde{\symbf{x}}_{i}, \tilde{\symbf{u}}_{i}\}
    \mid
    v_{i, l} \leq \tilde{\symbf{u}}_{i} < v_{i, l + 1}
  \right\},
\end{equation} for \(l = 1, \ldots, L_{i}\). Denote
\(N^{V}_{i, l} = \lvert V_{i, l} \rvert \mathop{/} 2\) (dividing by two
as \(V_{i, l}\) contains both the observation and the observation time).
The \(l\)\textsuperscript{th} 8-hourly fluid change \(\Delta_{i, l}\)
and corresponding observation time \(u_{i, l}\) can then be computed as
\begin{equation}
  \Delta_{i, l} = \sum_{s = 1}^{N^{V}_{i, l}} \tilde{x}_{i, s}, \,\, \text{s.t.} \,\, \tilde{x}_{i, s} \in V_{i, l}, \qquad
  u_{i, l} = \frac{1}{N^{V}_{i, l}} \sum_{s = 1}^{N^{V}_{i, l}} \tilde{u}_{i, s}, \,\, \text{s.t.} \,\, \tilde{u}_{i, s} \in V_{i, l}.
\end{equation} Finally, the 8-hourly cumulative fluid balance data are
computed by \(x_{i, l} = \sum_{s = 1}^{l} \Delta_{i, s}\), and we assume
they too are observed at \(u_{i, l}\).

\hypertarget{priors-and-justification-for-the-cumulative-fluid-submodel}{%
\section{Priors and justification for the cumulative fluid
submodel}\label{priors-and-justification-for-the-cumulative-fluid-submodel}}

The parameters for the gamma prior for \(\eta^{b}_{1, i}\) and
\(\eta^{a}_{1, i}\) are obtained by assuming that the 2.5-, 50-, and
97.5- percentiles are at 0.5, 5, and 20
\citep{belgorodski_rriskdistributions_2017}. A slope of \(0.5\)
(i.e.~the change in cumulative fluid balance per day) is unlikely but
possible due to missing data; a slope of \(20\) is also unlikely but
possible as extremely unwell patients can have very high respiratory
rates and thus require large fluid inputs.

The prior for the breakpoint \(\kappa_{i}\) is derived as follows.
Define \(u_{i, (1)} = \min(\symbf{u}_{i})\) and
\(u_{i, (n)} = \max(\symbf{u}_{i})\), with
\(r_{i} = u_{i, (n)} - u_{i, (1)}\). We reparameterise the breakpoint by
noting that \(\kappa_{i} = \kappa^{\text{raw}}_{i}r_{i} + u_{i, (1)}\),
where \(\kappa^{\text{raw}} \in [0, 1]\). We then set
\(\kappa^{\text{raw}}_{i} \sim \text{Beta}(5, 5)\) to regularise the
breakpoint towards the middle of each individual's stay in ICU. This is
crucial to ensure the submodel is identifiable when there is little
evidence of a breakpoint in the data. Note that this results in the
following analytic expression for \(\pd_{2}(\phi_{2 \cap 3})\)
\begin{equation}
  \pd_{3}(\phi_{2 \cap 3}) = \prod_{i = 1}^{N} \pd(\eta^{b}_{1, i}) \pd(\eta^{a}_{1, i}) \pd(\kappa_{i}), \,\, \text{with} \,\,\,
  \pd(\kappa_{i}) = \pd_{\kappa^{\text{raw}}_{i}}\left(\frac{\kappa_{i} - u_{i, (1)}}{r_{i}}\right) \frac{1}{r_{i}}
\end{equation} by the change of variables formula.

Specifying a prior for \(\eta_{0, i}\), the cumulative fluid balance at
\(\kappa_{i}\), is difficult because it too depends on the length of
stay. Instead, we reparameterise so that \(\eta_{0, i}\) is a function
of the y-intercept \(\eta_{0, i}^{\text{raw}}\). \begin{equation}
  \eta_{0, i} =
    (\eta_{0, i}^{\text{raw}} + \eta^{b}_{1, i} \kappa_{i}) \symbf{1}_{\{0 < \kappa_{i}\}} +
    (\eta_{0, i}^{\text{raw}} + \eta^{a}_{1, i} \kappa_{i}) \symbf{1}_{\{0 \geq \kappa_{i}\}}
\end{equation} We place a \(\text{LogNormal}(1.61, 0.47^2)\) prior on
\(\eta_{0, i}^{\text{raw}}\). These values are obtained assuming that,
\emph{a priori}, the \(2.5\%, 50\%\), and \(99\%\) percentiles of
\(\eta_{0, i}^{\text{raw}}\) are \(0.5, 5\), and \(15\) respectively
\citep{belgorodski_rriskdistributions_2017}. This is a broad prior that
reflects the numerous possible admission routes into the ICU. We expect
those admitted from the wards to have little pre-admission fluid data.
Those admitted from the operating theatre occasionally have their
in-theatre fluid input recorded after admission into the ICU, with no
easy way to distinguish these records in the data.

\hypertarget{analytic-form-for-the-survival-function}{%
\section{Analytic form for the survival
function}\label{analytic-form-for-the-survival-function}}

The hazard at arbitrary time \(t\) is

\begin{gather*}
  h_{i}(t) = \gamma t^{\gamma - 1} \exp\left\{\symbf{w}_{i}^{\top}\symbf{\theta} + \alpha \frac{\partial}{\partial t} m_{i}(t)\right\} \\
  m_{i}(t) = \eta_{0, i} + \eta^{b}_{1, i}(t - \kappa_{i})\symbf{1}_{\{t < \kappa_{i}\}} + \eta^{a}_{1, i}(t - \kappa_{i})\symbf{1}_{\{t \geq \kappa_{i}\}} \\
  \frac{\partial}{\partial t} m_{i}(t) = \eta^{b}_{1, i}\symbf{1}_{\{t < \kappa_{i}\}} + \eta^{a}_{1, i}\symbf{1}_{\{t \geq \kappa_{i}\}}.
\end{gather*} Then, for \(t > \kappa_{i}\), the cumulative hazard is
\begin{align*}
  \int_{0}^{t} h_{i}(u) \text{d}u
  &= \int_{0}^{t}
    \gamma u^{\gamma - 1}
    \exp\left\{
      \symbf{w}_{i}^{\top}\symbf{\theta} +
      \alpha \eta^{b}_{1, i}\symbf{1}_{\{u < \kappa_{i}\}} +
      \alpha \eta^{a}_{1, i}\symbf{1}_{\{u \geq \kappa_{i}\}}
    \right\}
    \text{d}u \\
  &= \gamma \exp\{\symbf{w}_{i}^{\top}\symbf{\theta}\}
    \int_{0}^{t}
      u^{\gamma - 1}
      \exp\left\{
        \alpha \eta^{b}_{1, i}\symbf{1}_{\{u < \kappa_{i}\}} +
        \alpha \eta^{a}_{1, i}\symbf{1}_{\{u \geq \kappa_{i}\}}
      \right\}
    \text{d}u \\
  &= \gamma \exp\{\symbf{w}_{i}^{\top}\symbf{\theta}\}
    \left[
      \int_{0}^{\kappa_{i}}
        u^{\gamma - 1}
        \exp\left\{
          \alpha \eta^{b}_{1, i}
        \right\}
      \text{d}u
      +
      \int_{\kappa_{i}}^{t}
        u^{\gamma - 1}
        \exp\left\{
          \alpha \eta^{a}_{1, i}
        \right\}
      \text{d}u
    \right] \\
  &= \exp\{\symbf{w}_{i}^{\top}\symbf{\theta}\}
    \left[
      \exp\left\{
        \alpha \eta^{b}_{1, i}
      \right\}
      \kappa_{i}^{\gamma}
      +
      \exp\left\{
        \alpha \eta^{a}_{1, i}
      \right\}
      (t^{\gamma} - \kappa_{i}^{\gamma})
    \right]
\end{align*}

and for \(t < \kappa_{i}\)

\begin{align*}
  \int_{0}^{t} h_{i}(u) \text{d}u
  &= \gamma \exp\{\symbf{w}_{i}^{\top}\symbf{\theta}\}
    \left[
      \int_{0}^{t}
        u^{\gamma - 1}
        \exp\left\{
          \alpha \eta^{b}_{1, i}
        \right\}
      \text{d}u
    \right] \\
  &= \exp\{\symbf{w}_{i}^{\top}\symbf{\theta}\}
    \left[
      \exp\left\{
        \alpha \eta^{b}_{1, i}
      \right\}
      t_{i}^{\gamma}
    \right] \\
  &= t_{i}^{\gamma} \exp\{\symbf{w}_{i}^{\top}\symbf{\theta} + \alpha \eta^{b}_{1, i}\}.
\end{align*} The survival functions then have corresponding definitions
for \(t > \kappa_{i}\) and \(t < \kappa_{i}\) as
\(S_{i}(t) = \exp\{-\int_{0}^{t} h_{i}(u) \text{d}u\}\).

\hypertarget{p2-prior-justification}{%
\section{Survival submodel prior
justification}\label{p2-prior-justification}}

Our prior for \((\gamma, \alpha, \symbf{\theta})\) must result in a
plausible distribution for \(\pd_{2, i}(T_{i} \mid d_{i} = 1)\), and a
reasonable balance between \(d_{i} = 1\) and \(d_{i} = 0\) events. The
primary concern is unintentionally specifying a prior for which the bulk
of \(\pd_{2, i}(T_{i} \mid d_{i} = 1)\) is very close to zero. In
addition, certain extreme configurations of
\((\gamma, \alpha, \symbf{\theta})\) cause issues for the
methodology of \citet{crowther_simulating_2013}, particularly the
numerical root finding and numerical integration steps. We would like to
rule out such extreme configurations \emph{a priori}. Ideally we would
encode this information a joint prior for
\((\gamma, \alpha, \symbf{\theta})\), but specifying the
appropriate correlation structure for these parameters is prohibitively
challenging. Instead we focus on specifying appropriate marginals for
each of \(\gamma, \alpha\), and \(\symbf{\theta}\), and create
visual prior predictive checks
\citep{gabry_visualization_2019, gelman_bayesian_2020} to ensure the
induced prior for \((T_{i}, d_{i})\) is acceptable.

Before justifying our chosen marginal prior, we note that the
\(\exp\{\symbf{x}_{i}\theta + \alpha \frac{\partial}{\partial T_{i}} m_{i}(T_{i})\}\)
term implies that the priors for \(\theta\) and \(\alpha\) are on the
log-scale. Hence the magnitude of these parameters must be small,
otherwise all event times would be very near zero or at infinity. The
asymmetric effect of the transformation from the log scale also implies
that symmetric priors are not obviously sensible. From these
observations we deduce that \(\theta\) and \(\alpha\) must not be too
large in magnitude, however if they are negative then they can be
slightly larger. Hence, we specify the skew-normal priors detailed in
Section \ref{survival-submodel-pd_2}, noting that the skewness parameter
for \(\alpha\) is smaller, because
\(\frac{\partial}{\partial T_{i}} m_{i}(T_{i})\) is strictly positive
and typically between 0.5 and 20, whilst \(\symbf{w}_{i}\) is
standardised to be approximately standard normal. Lastly, if \(\gamma\)
is too far away from \(1\) (in either direction), then the event times
are very small either because the hazard increases rapidly
(\(\gamma \gg 1\)), or because almost all of the cumulative hazard is in
the neighbourhood of 0 (\(\gamma \ll 1\)). We specify a gamma
distribution for \(\gamma\) with the \(1\)\textsuperscript{th}-,
\(50\)\textsuperscript{th}-, and \(99\)\textsuperscript{th}-percentiles
of \(\pd_{2}(\gamma)\) at \(0.2, 1\), and \(2\), allowing for a wide
range of hazard shapes, but removing many of the extremes.

\hypertarget{estimating-submodel-prior-marginal-distributions}{%
\section{Estimating submodel prior marginal
distributions}\label{estimating-submodel-prior-marginal-distributions}}

For \(\pd_{1}\), we note that
\(\pd_{1}(\phi_{1 \cap 2}) = \prod_{i = 1}^{N}\pd_{1, i}(T_{i}, d_{i})\),
and that \(\pd_{1, i}(T_{i}, d_{i})\) conditions on each individual's
length of stay (in specifying the location of the knots), as well as the
range, mean, and standard deviation of the P/F data (by standardising
\(\tilde{z}_{i, j}\)). Simple Monte Carlo samples are drawn from
\(\pd_{1}(\phi_{1 \cap 2})\) and used to estimate
\(\widehat{\pd}_{1}(\phi_{1 \cap 2})\). Under the second submodel we
obtain samples from \(\pd_{2}(\phi_{1 \cap 2}, \phi_{2 \cap 3})\) using
the methodology of \citet{crowther_simulating_2013} as implemented in
\texttt{simsurv} \citep{brilleman_simsurv_2021}. These samples are use
to estimate \(\widehat{\pd}_{2}(\phi_{1 \cap 2}, \phi_{2 \cap 3})\).

\hypertarget{pf-submodel}{%
\subsection{P/F submodel}\label{pf-submodel}}

We approximate \(\pd_{1}(\phi_{1 \cap 2})\) using a mixture of discrete
and continuous distributions, with a discrete spike at \(C_{i}\) for the
censored events and a beta distribution for the (rescaled) event times.
Monte Carlo samples of \(T_{i}\) and \(d_{i}\) are obtained from
\(\pd_{1, i}(T_{i}, d_{i})\) by drawing \(\beta_{0, i}\) and
\(\symbf{\zeta}_{i}\) from their respective prior distributions and
then solving \eqref{eqn:event-time-model-def}. Denoting the estimated
mixture weight \(\widehat{\pi}_{i} \in [0, 1]\), the density estimate is
\begin{equation}
  \widehat{\pd}_{1, i}(T_{i}, d_{i}) =
    \widehat{\pi}_{i} \, \text{Beta}\left(\frac{T_{i}}{C_{i}}; \widehat{a}, \widehat{b}\right) \frac{1}{C_{i}} \symbf{1}_{\{d_{i} = 1\}} +
    (1 - \widehat{\pi}_{i}) \symbf{1}_{\{d_{i} = 0, T_{i} = C_{i}\}}
  \label{eqn:pf-event-time-prior-dist}
\end{equation} where \(\widehat{\pi}_{i}, \widehat{a}_{i}\) and
\(\widehat{b}_{i}\) are maximum likelihood estimates obtained using the
prior samples. Examples of \(\widehat{\pd}_{1, i}(T_{i}, d_{i})\) for a
subset of individuals are displayed in Figure \ref{fig:pf_prior_fit}.

\begin{figure}

{\centering \includegraphics{plots/mphi/mimic-example/pf-prior-plot-small.pdf} 

}

\caption{Fitted distribution (curve) and Monte Carlo samples drawn from $\pd_{1}(\phi_{1 \cap 2})$ (histogram) for a subset of the individuals in the cohort. The height of the atom at $C_{i}$ (red bar and point) has been set to $1 - \widehat{\pi}_{i}$}\label{fig:pf_prior_fit}
\end{figure}

\hypertarget{survival-submodel}{%
\subsection{Survival submodel}\label{survival-submodel}}

Our estimate of \(\pd_{2}(\phi_{1 \cap 2}, \phi_{2 \cap 3})\) relies on
the fact that \begin{equation}
  \pd_{2}(\phi_{1 \cap 2}, \phi_{2 \cap 3}) = \prod_{i = 1}^{N}\pd_{2, i}(T_{i}, d_{i}, \kappa_{i}, \eta^{b}_{1, i}, \eta^{a}_{1, i}).
\end{equation} As such we estimate
\(\pd_{2, i}(T_{i}, d_{i}, \kappa_{i}, \eta^{b}_{1, i}, \eta^{a}_{1, i})\)
for each individual and take the product of these estimates. Drawing
samples from
\(\pd_{2, i}(T_{i}, d_{i}, \kappa_{i}, \eta^{b}_{1, i}, \eta^{a}_{1, i})\)
is challenging: we use the approach proposed by
\citet{crowther_simulating_2013} as implemented in
\citet{brilleman_simsurv_2021}. Inspecting the samples reveals
correlation between \((T_{i}, d_{i})\) and
\((\kappa_{i}, \eta^{b}_{1, i}, \eta^{a}_{1, i})\) that we would like to
capture in our estimate. To do so, we fit a mixture of multivariate
normal distributions to transformations of the continuous parameters
with support on \(\mathbb{R}\), \begin{equation}
  \begin{gathered}
    \tilde{T}_{i} = \text{Logit}\left(\frac{T_{i}}{C_{i}}\right), \quad
    \tilde{\kappa}_{i} = \text{Logit}\left(\frac{\kappa_{i} - u_{i, (1)}}{u_{i, (n)} - u_{i, (1)}}\right), \\
    \tilde{\eta}^{b}_{1, i} = \log(\eta^{b}_{1, i}), \quad
    \tilde{\eta}^{a}_{1, i} = \log(\eta^{a}_{1, i}).
  \end{gathered}
\end{equation} The resulting density estimate, with estimated mixture
weight \(\widehat{\theta}_{i} \in [0, 1]\), is \begin{align*}
  \widehat{\pd}_{2}(T_{i}, d_{i}, \kappa_{i}, \eta^{b}_{1, i}, \eta^{a}_{1, i}) =
    \Big[&
      (\widehat{\theta}_{i})
      \text{N}\left(
        \left[\tilde{T}_{i}, \tilde{\kappa}_{i}, \tilde{\eta}^{b}_{1, i}, \tilde{\eta}^{a}_{1, i} \right]^{\mathsf{T}}; \widehat{\mu}_{1, i}, \widehat{\Sigma}_{1, i}
      \right)
      \symbf{1}_{\{d_{i} = 1\}}
      + \\
      & (1 - \widehat{\theta}_{i})
      \text{N}\left(\left[\tilde{\kappa}_{i}, \tilde{\eta}^{b}_{1, i}, \tilde{\eta}^{a}_{1, i} \right]^{\mathsf{T}}; \widehat{\mu}_{0, i}, \widehat{\Sigma}_{0, i} \right)
      \symbf{1}_{\{d_{i} = 0, T_{i} = C_{i}\}}
    \Big]
    J_{i},
\end{align*} where
\(\widehat{\theta}_{i}, \widehat{\mu}_{1, i}, \widehat{\Sigma}_{1, i}, \widehat{\mu}_{0, i}\),
and \(\widehat{\Sigma}_{0, i}\) are maximum likelihood estimates, and
the Jacobian correction \(J_{i}\) is \begin{equation}
  J_{i} = \left[
    \left(
      \frac{1}{C_{i} - T_{i}} +
      \frac{1}{T_{i}}
    \right)^{d_{i}}
    \left(
      \frac{1}{u_{i, (n)} - \kappa_{i}} +
      \frac{1}{\kappa_{i} - u_{i, (1)}}
    \right)
    \left(
      \frac{1}{\eta^{b}_{1, i}}
    \right)
    \left(
      \frac{1}{\eta^{a}_{1, i}}
    \right)
  \right].
\end{equation}

We assess the fit of this estimate by drawing samples from
\(\widehat{\pd}_{2, i}(\phi_{1 \cap 2}, \phi_{2 \cap 3})\) and comparing
them to the Monte Carlo samples drawn using \texttt{simsurv}. Our visual
assessment is displayed in Figure \ref{fig:surv_prior_plot_fit} for
individual \(i = 19\). The normal approximation seems to fit the samples
well, with the shape of \(\pd_{2, i}(T_{i} \mid d_{i} = 1)\) closely
matching that of the Monte Carlo samples, and with a similar mix of
\(d_{i} = 0\) and \(d_{i} = 1\) samples.

We also require an estimate of \(\pd_{2}(\phi_{1 \cap 2})\) for
experiments discussed in Section \ref{results}. This is obtained using
the samples generated under the survival submodel prior and the
methodology of Section \ref{pf-submodel}. The raw samples and fit are
displayed in Figure \ref{fig:surv_prior_phi_12_marginal_plot_fit}.

\begin{figure}

{\centering \includegraphics{plots/mphi/mimic-example/submodel-2-phi-12-marginal-fit-plot-small.pdf} 

}

\caption{Fitted distribution (curve) and Monte Carlo samples drawn from $\pd_{2}(\phi_{1 \cap 2})$ (histogram) for a subset of the individuals in the cohort. The height of the atom at $C_{i}$ (red bar and point) has been set to $1 - \widehat{\pi}_{i}$}\label{fig:surv_prior_phi_12_marginal_plot_fit}
\end{figure}

\begin{figure}

{\centering \includegraphics{plots/mphi/mimic-example/p3-prior-pairs/pairs-19.png} 

}

\caption{Monte Carlo (MC) samples from $\pd_{2, i}(\phi_{1 \cap 2}, \phi_{2 \cap 3})$ obtained using \texttt{simsurv} and samples from the fitted normal approximation (NA) for $i = 19$. The panels on the off diagonal elements contain a 2D kernel density estimate for $d_{i} = 1$ and the samples for $d_{i} = 0$. Diagonal and lower-triangular panels are on their original scales, whilst the upper-triangular panels are on the log scale.}\label{fig:surv_prior_plot_fit}
\end{figure}

\hypertarget{cohort-selection-criteria}{%
\section{Cohort selection criteria}\label{cohort-selection-criteria}}

This appendix details the cohort selection criteria and our rationale
for them. In the text we speak of the \(i\)\textsuperscript{th}
individual. This is because in our final data set (the data that results
from applying the following criteria) we are dealing with unique
individuals, however some individuals in MIMIC have multiple ICU stays.
In this appendix \(i\) represents a single stay in ICU.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Each ICU stay must have at least 12 \pfratio~observations
  (\(J_{i} \geq 12\)), with the first 6 being greater than 350
  (\(z_{i, j} > 350\) for \(j = 1, \dots, 1\)).

  \begin{itemize}
  \tightlist
  \item
    This is to ensure we have enough data to fit a B-spline with 7
    internal knots. The restriction on the first 6 observations is to
    avoid selecting those who have already started to experience
    respiratory failure prior to ICU admission.
  \end{itemize}
\item
  The time between any 2 consecutive \pfratio~observations cannot exceed
  2 days.

  \begin{itemize}
  \tightlist
  \item
    This is because we believe this lack of \pfratio~observations is
    likely to be an error in the data: e.g.~what appears as a single ICU
    stay in the data is actually two or more separate stays.
  \end{itemize}
\item
  The fluid observations must be after ICU admission (some observations
  are entered as `Pre-admission intake') and cannot be associated with
  fluid administered in the operating room (OR). Note that this does not
  mean all OR fluid administrations are removed, as some are
  mis-labelled.
\item
  There must be sufficient temporal overlap between the fluid data and
  the \pfratio~data. Specifically, \begin{equation}
   \frac{
     \max\left\{0, \min\left[\max(\symbf{t}_{i}), \max(\tilde{\symbf{u}}_{i})\right] - \max\left[\min(\symbf{t}_{i}), \min(\tilde{\symbf{u}}_{i})\right]\right\}
   } {
     \max\left[\max(\symbf{t}_{i}), \max(\tilde{\symbf{u}}_{i})\right] - \min\left[\min(\symbf{t}_{i}), \min(\tilde{\symbf{u}}_{i})\right]
   }
   > 0.9
   \label{eqn:overlap-def}
    \end{equation}

  \begin{itemize}
  \tightlist
  \item
    The numerator of \eqref{eqn:overlap-def} is strictly positive, and
    the denominator ensures that the quantity is bounded between 0 and
    1.
  \item
    We cannot investigate the relationship between the rate of fluid
    intake and respiratory failure if the latter occurs without
    sufficient fluid data surrounding the event.
  \end{itemize}
\end{enumerate}

\hypertarget{baseline-covariate-information}{%
\section{Baseline covariate
information}\label{baseline-covariate-information}}

The baseline covariate vector \(\symbf{w}_{i}\) contains the median
of the measurements taken in the first 24 hours of the ICU stay, which
are then stardardised, of the following covariates: Anion gap,
Bicarbonate, Creatinine, Chloride, Glucose, Hematocrit, Hemoglobin,
Platelet, Partial thromboplastin time, International normalized ratio,
Prothrombin time, Sodium, blood Urea nitrogen, White blood cell count,
Age at ICU admission, and Gender.

\hypertarget{one-at-a-time}{%
\section{\texorpdfstring{Updating \(\phi_{1 \cap 2}\) and
\(\phi_{2 \cap 3}\) in stage two
individual-at-a-time}{Updating \textbackslash phi\_\{1 \textbackslash cap 2\} and \textbackslash phi\_\{2 \textbackslash cap 3\} in stage two individual-at-a-time}}\label{one-at-a-time}}

The parallel sampler described in Section \ref{parallel-sampler} is a
MH-within-Gibbs scheme, with each iteration sampling from the
conditionals of the melded model \begin{equation}
\begin{gathered}
  \pd_{\text{meld}}(\phi_{1 \cap 2}, \psi_{1} \mid \symbf{Y}, \psi_{2}, \phi_{2 \cap 3}, \psi_{3}), \\
  \pd_{\text{meld}}(\phi_{2 \cap 3}, \psi_{3} \mid \symbf{Y}, \phi_{1 \cap 2}, \psi_{1}, \psi_{2}), \\
  \pd_{\text{meld}}(\psi_{2} \mid \symbf{Y}, \phi_{1 \cap 2}, \psi_{1}, \phi_{2 \cap 3}, \psi_{3}).
\end{gathered}
\end{equation} We would like to update the first two conditionals
`individual-at-a-time'. For simplicity we will assume the pooled prior
is formed using product-of-experts pooling, and focus on the first
conditional
\(\pd_{\text{meld}}(\phi_{1 \cap 2}, \psi_{1} \mid \symbf{Y}, \psi_{2}, \phi_{2 \cap 3}, \psi_{3})\).
Similar arguments apply to the second conditional and other pooling
types. Recall that \begin{equation}
\begin{aligned}
\phi_{1 \cap 2} &=
  (\phi_{1 \cap 2, 1}, \ldots, \phi_{1 \cap 2, N})  \\
  &= (\phi_{1 \cap 2, 1}(\chi_{1, 1}), \ldots, \phi_{1 \cap 2, N}(\chi_{1, N})) \\
  &= \left((T_{1}, d_{1}), \ldots, (T_{N}, d_{N})\right),
\end{aligned}
\end{equation} and
\(\psi_{1} = (\psi_{1, 1}, \ldots, \psi_{1, N}) = (\omega_{i})_{i = 1}^{N}\).
We would like to update \(\phi_{1 \cap 2}\) and \(\psi_{1}\) by updating
\((T_{i}, d_{i}, \omega_{i})\) for one individual at a time, for a total
of \(N\) `sub-steps'.

\hypertarget{sub-step-1}{%
\subsection{Sub-step 1}\label{sub-step-1}}

Suppose we are at step \(t - 1\) of the Markov chain, and we are
proposing values for step \(t\). Without any loss of generality we
assume that we are updating individual \(i = 1\) in sub-step 1 -- in
practice we update the individuals in a random order for each iteration
of the MH-within-Gibbs scheme.

Our target is \begin{equation}
  \pd_{1}(\chi_{1, 1}, \psi_{1, 1} \mid Y_{1}, (\chi_{1, i}, \psi_{1, i})_{i = 2}^{N})
  \pd_{2}(\phi_{1 \cap 2, 1}(\chi_{1, 1}) \mid Y_{2}, (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = 2}^{N}, \psi_{2}, \phi_{2 \cap 3}).
  \label{eqn:sub-step-target}
\end{equation} The model, detailed in Section
\ref{pf-ratio-submodel-b-spline-pd_1}, uses the conditional independence
between individuals to factorise such that \begin{equation}
  \pd_{1}(\chi_{1}, \psi_{1} \mid Y_{1}) = \prod_{i = 1}^{N} \pd_{1}(\chi_{1, i}, \psi_{1, i} \mid Y_{1}),
\end{equation} which implies\footnote{This property is also true of
  \(\pd_{3}\).}
\((\chi_{1, i}, \psi_{1, i}\indep \chi_{1, i^{'}}, \psi_{1, i^{'}}) \mid Y_{1}\)
for \(i \neq i^{'}\), hence \begin{equation}
  \pd_{1}(\chi_{1, 1}, \psi_{1, 1} \mid Y_{1}, (\chi_{1, i}, \psi_{1, i})_{i = 2}^{N}) =
  \pd_{1}(\chi_{1, 1}, \psi_{1, 1} \mid Y_{1}).
\end{equation} It will be convenient to rewrite
\eqref{eqn:sub-step-target} as \begin{equation}
  \pd_{1}(\chi_{1, 1}, \psi_{1, 1} \mid Y_{1})
  \frac {
    \pd_{2}(\phi_{1 \cap 2, 1}(\chi_{1, 1}), (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = 2}^{N}, \psi_{2}, \phi_{2 \cap 3} \mid Y_{2})
  } {
    \pd_{2}((\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = 2}^{N}, \psi_{2}, \phi_{2 \cap 3} \mid Y_{2})
  }.
  \label{eqn:sub-step-one-target}
\end{equation}

Suppose there are \(K_{1}\) stage one samples from
\(\pd_{1}(\chi_{1}, \psi_{1} \mid Y_{1})\), and each sample has a
corresponding \(\phi_{1 \cap 2}\). We propose \(\psi_{1, 1}^{*}\) and
\(\chi_{1, 1}^{*}\) (hence \(\phi_{1 \cap 2, 1}^{*})\) by sampling a
random integer \(k_{1}^{*}\) from \(\{1, \ldots, K_{1}\}\), retrieving
the corresponding values of \(\psi_{1}\) and \(\chi_{1}\), and ignoring
\((\chi_{1, 2}, \ldots, \chi_{1, N}, \psi_{1, 2}, \ldots, \psi_{1, N})\).
Given stage one samples from the correct stationary distribution,
obtained from the post-warmup samples of a well mixed set of Markov
chains, such a proposal mechanism is approximately equivalent to
proposing from \(\pd_{1}(\chi_{1, 1}, \psi_{1, 1} \mid Y_{1})\). The
quality of the approximation depends on the quality of the stage one
samples. Under this proposal, noting that the denominator term in
\eqref{eqn:sub-step-one-target} does not depend on the proposed
parameters, the acceptance probability for this sub-step is
\begin{equation}
\begin{aligned}
\alpha&\left((\chi_{1, 1}^{*}, \psi_{1, 1}^{*})_{k_{1}^{*}}, (\chi_{1, 1}, \psi_{1, 1}) \right) \\
&=
\frac {
  \pd_{1}(\chi_{1, 1}^{*}, \psi_{1, 1}^{*} \mid Y_{1})
  \pd_{2}(\phi_{1 \cap 2, 1}(\chi_{1, 1}^{*}), (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = 2}^{N}, \phi_{2 \cap 3}, \psi_{2} \mid Y_{2})
} {
  \pd_{1}(\chi_{1, 1}, \psi_{1, 1} \mid Y_{1})
  \pd_{2}(\phi_{1 \cap 2, 1}(\chi_{1, 1}), (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = 2}^{N}, \phi_{2 \cap 3}, \psi_{2} \mid Y_{2})
} \\
&\qquad \times
\frac{
  \pd_{1}(\chi_{1, 1}, \psi_{1, 1} \mid Y_{1})
} {
  \pd_{1}(\chi_{1, 1}^{*}, \psi_{1, 1}^{*} \mid Y_{1})
} \\
&= \frac {
  \pd_{2}(\phi_{1 \cap 2, 1}(\chi_{1, 1}^{*}), (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = 2}^{N}, \phi_{2 \cap 3}, \psi_{2} \mid Y_{2})
} {
  \pd_{2}(\phi_{1 \cap 2, 1}(\chi_{1, 1}), (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = 2}^{N}, \phi_{2 \cap 3}, \psi_{2} \mid Y_{2})
},
\end{aligned}
\end{equation} which does not depend on \(\pd_{1}\). We store the value
of \(k_{1}^{*}\) associated with the proposal (if it is accepted) in
order to resample the stage one values of \(\psi_{1, 1}\) to reflect the
information in the other submodels.

\hypertarget{sub-step-n}{%
\subsection{\texorpdfstring{Sub-step
\(n\)}{Sub-step n}}\label{sub-step-n}}

At sub-step \(n\), for \(1 < n \leq N\) with
\((\chi_{1, i})_{i = N + 1}^{N} = \varnothing\), we have updated
\(((\chi_{1, i}, \psi_{1, i})_{i = 1}^{n - 1})\) to
\(((\chi_{1, i}^{[t]}, \psi_{1, i}^{[t]})_{i = 1}^{n - 1})\). Thus the
target is 
\begin{equation}
\begin{aligned}
  & \pd_{1}  \left(\chi_{1, n}, \psi_{1, n} \mid Y_{1}, (\chi_{1, i}^{[t]}, \psi_{1, i}^{[t]})_{i = 1}^{n - 1}, (\chi_{1, i}, \psi_{1, i})_{i = n + 1}^{N}\right) \\
  & \qquad \times \pd_{2} \left(\phi_{1 \cap 2, n}(\chi_{1, n}) \mid Y_{2}, \psi_{2}, \phi_{2 \cap 3}, (\phi_{1 \cap 2, i}(\chi_{1, i}^{[t]}))_{i = 1}^{n - 1}, (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = n + 1}^{N}\right)
  \\
  & = \pd_{1} \left(\chi_{1, n}, \psi_{1, n}, \mid Y_{1} \right)
  \\
  & \qquad \times
  \frac {
    \pd_{2} \left(\phi_{1 \cap 2, n}(\chi_{1, n}), \psi_{2}, \phi_{2 \cap 3}, (\phi_{1 \cap 2, i}(\chi_{1, i}^{[t]}))_{i = 1}^{n - 1}, (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = n + 1}^{N} \mid Y_{2}\right)
  } {
    \pd_{2} \left(\psi_{2}, \phi_{2 \cap 3}, (\phi_{1 \cap 2, i}(\chi_{1, i}^{[t]}))_{i = 1}^{n - 1}, (\phi_{1 \cap 2, i}(\chi_{1, i}))_{i = n + 1}^{N} \mid Y_{2}\right)
  }
\end{aligned}
\label{eqn:two-to-N-target} 
\end{equation}

We propose \(\chi_{1, n}^{*}, \psi_{1, n}^{*}\) in the same
manner as the previous section. The corresponding acceptance probability
is \begin{equation}
\begin{aligned}
\alpha ((\chi_{1, n}^{*}, \psi_{1, n}^{*}), & (\chi_{1, n}, \psi_{1, n}) )  \\
= &\frac {
  \pd_{1} \left(\chi_{1, n}^{*}, \psi_{1, n}^{*} \mid Y_{1} \right)
} {
  \pd_{1} \left(\chi_{1, n}, \psi_{1, n} \mid Y_{1} \right)
} \\
& \times \frac {
  \pd_{2}(\phi_{1 \cap 2, n}(\chi_{1, n}^{*}), \psi_{2}, \phi_{2 \cap 3}, (\chi_{1, i}^{[t]})_{i = 1}^{n - 1}, (\chi_{1, i})_{i = n + 1}^{N} \mid Y_{2})
} {
  \pd_{2}(\phi_{1 \cap 2, n}(\chi_{1, n}), \psi_{2}, \phi_{2 \cap 3}, (\chi_{1, i}^{[t]})_{i = 1}^{n - 1}, (\chi_{1, i})_{i = n + 1}^{N} \mid Y_{2})
} \\
& \times \frac{
  \pd_{1}(\chi_{1, n}, \psi_{1, n} \mid Y_{1})
} {
  \pd_{1}(\chi_{1, n}^{*}, \psi_{1, n}^{*} \mid Y_{1})
} \\
= &\frac {
  \pd_{2}(\phi_{1 \cap 2, n}(\chi_{1, n}^{*}), \psi_{2}, \phi_{2 \cap 3}, (\chi_{1, i}^{[t]})_{i = 1}^{n - 1}, (\chi_{1, i})_{i = n + 1}^{N} \mid Y_{2})
} {
  \pd_{2}(\phi_{1 \cap 2, n}(\chi_{1, n}), \psi_{2}, \phi_{2 \cap 3}, (\chi_{1, i}^{[t]})_{i = 1}^{n - 1}, (\chi_{1, i})_{i = n + 1}^{N} \mid Y_{2})
}.
\end{aligned}
\end{equation}
