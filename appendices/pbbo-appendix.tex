\chapter{Supplementary material to Chapter \ref{chp:pbbo}}
\label{pbbo-appendix-full}

\hypertarget{importance-sampling}{%
\section{Importance sampling}\label{importance-sampling}}

Appropriate importance distributions are crucial to obtaining an
accurate and low variance estimate of
\(D(\lambda \mid \symbf{X})\). For values of \(\lambda\) far from
optimal, \(\Pd(Y \mid \lambda, \symbf{X})\) can differ considerably
from \(\tc(Y \mid \symbf{X})\). Given a specific \(X_{r}\) we
require an importance distribution \(\Q_{r}(Y)\) that places substantial
mass in the high probability regions of both \(\tc(Y \mid X_{r})\) and
\(\Pd(Y \mid \lambda, X_{r})\), as it is in these regions that
\(d(\cdot, \cdot)\) is largest. But we cannot exert too much effort on
finding these densities as they are specific to each value of
\(\lambda\), and must be found anew for each \(\lambda\).

We use three quantities to guide our choice of \(\Q_{r}(Y)\), these
being the support \(\mathcal{Y}\), the samples
\(\symbf{y}_{r}^{(\Pd)} \sim \Pd(Y \mid \lambda, X_{r})\), and the
samples \(\symbf{y}_{r}^{(\tc)} \sim \tc(Y \mid X_{r})\). Of
primary concern is the support. If \(\mathcal{Y} = \mathbb{R}\) then we
use a mixture of Student-\(t_{5}\) distributions; for
\(\mathcal{Y} = \mathbb{R} = (0, \infty)\) we employ a mixture of gamma
distributions; and for \(\mathcal{Y} = (0, a]\) with known \(a\), we opt
for a mixture of Beta distributions with a discrete component at
\(Y = a\). The parameters of the mixture components are estimated using
the method of moments. Specifically, denoting the empirical mean of
\(\symbf{y}_{r}^{(\Pd)}\) as \(\hat{\mu}^{(\Pd)}\) and the
empirical variance by \(\hat{v}^{(\Pd)}\), with \(\hat{\mu}^{(\tc)}\)
and \(\hat{v}^{(\tc)}\) defined correspondingly for
\(\symbf{y}_{r}^{(\tc)}\), Table
\ref{tab:importance-sampling-appendix-table} details our method of
moments estimators for the mixture components.

In this chapter we limit ourselves to one dimensional \(\mathcal{Y}\),
where importance sampling is mostly well behaved or can be tamed using a
reasonable amount of computation. This covers many models, and with the
covariate-specific target it includes regression models. It is harder to
elicit \(\tc(Y \mid \symbf{X})\) for higher dimensional data
spaces, and the difficulties with higher dimensional importance sampling
are well known.

\begin{landscape}
\input{tex-input/pbbo/pbbo-methodology/0071-importance-sampling-appendix-table.tex}
\end{landscape}

\hypertarget{evaluating-dlambda-mid-symbfx}{%
\section{\texorpdfstring{Evaluating
\(D(\lambda \mid \symbf{X})\)}{Evaluating D(\textbackslash lambda \textbackslash mid \textbackslash symbf\{X\})}}\label{evaluating-dlambda-mid-symbfx}}

For both numerical stability and optimisation performance
\citep{eriksson_scalable_2021, snoek_input_2014} we evaluate
\(D(\lambda \mid \symbf{X})\) on the log scale. This is because far
from optimal values of \(\lambda\) have corresponding
\(D(\lambda \mid \symbf{X})\) many orders of magnitude larger than
near optimal values of \(\lambda\). Furthermore, the Gaussian process
approximation that underlies Bayesian optimisation assumes constant
variance, necessitating a log or log-like transformation.

Suppose again that we sample
\(\symbf{y}_{r}^{(\Pd)} \sim \Pd(Y \mid \lambda, X_{r})\), from
which we form the ECDF
\(\hat{\Pd}(Y \mid \lambda, X_{r}, \symbf{y}_{r}^{(\Pd)})\). We
also select an appropriate importance distribution \(\Q_{r}(Y)\) and
density \(\q_{r}(Y)\) using the preceding Appendix \ref{importance-sampling}, and
sample importance points \((y_{i, r})_{i = 1}^{I_{r}} \sim \Q_{r}(Y)\).
Define the intermediary quantity \(z(y_{i, r})\) as
\input{tex-input/pbbo/pbbo-methodology/0020-log-discrep-func-defs.tex}\noindent
and then rewrite Equation
\eqref{eqn:practical-discrep-definition-covariate-importance} to read
\input{tex-input/pbbo/pbbo-methodology/0022-importance-discrepancy-covariate-definition.tex}\noindent
All \(\log(\sum \exp\{\cdot\})\) terms are computed using the
numerically stable form \citep{blanchard_accurately_2021}.

Accurately evaluating \(\log(d(\cdot, \cdot))\) in Equation
\eqref{eqn:log-discrep-func-def} involves managing the discrete nature
of the ECDF (that it returns exactly zero or one for some inputs), and
using specialised functions for each discrepancy to avoid issues with
floating point arithmetic. We compute
\(\log(d^{\text{CvM}}(\cdot, \cdot))\) using
\input{tex-input/pbbo/computing-log-discrep-functions/0010-computing-log-cvm.tex}\noindent
where \(\mathcal{T}(y_{i, r}) = \log(\tc(y_{i, r}))\). The log-CDF
(LCDF) is often more numerically accurate for improbable values of
\(y_{i, r}\), and so our methodology assumes that it is this LCDF form
in which the target distribution is supplied. However, because the ECDF
can return exact zero/one values there is no way to perform this
computation on the log scale. We thus employ high precision floating
point numbers when exponentiating the LCDF values, using \texttt{Rmpfr}
\citep{maechler_rmpfr_2021}, to avoid evaluating \(\log(0)\).

For \(\log(d^{\text{AD}}(\cdot, \cdot))\), additional care must be taken
as the denominator of \(d^{\text{AD}}\) in Equation
\eqref{eqn:discrepancies-definitions} tends to underflow to zero. Thus
we evaluate it using
\input{tex-input/pbbo/computing-log-discrep-functions/0011-computing-log-ad.tex}\noindent
where \(\texttt{log1mexp}(x) = \log(1 - \exp\{-x\})\) is implemented by
the \texttt{Rmpfr} package \citep{maechler_accurately_2012}. Such
precision is necessary for improbably large values of \(y_{i, r}\) under
\(\tc\), as the CDF/LCDF often rounds to 1/0 (respectively). It is not
always feasible to evaluate Equation \eqref{eqn:computing-log-ad} with
sufficient accuracy to avoid under/over-flow issues -- it requires a
high-precision implementation of \(\mathcal{T}(y_{i, r})\) for extreme
\(y_{i, r}\) and many additional bits of precision for both \(y_{i, r}\)
and the result. In these settings we revert to
\(\log(d^{\text{CvM}}(\cdot, \cdot))\).

\hypertarget{additional-information-for-the-preece-baines-example}{%
\section{Additional information for the Preece-Baines
example}\label{additional-information-for-the-preece-baines-example}}

\hypertarget{hartmann_flexible_2020-priors}{%
\subsection{\texorpdfstring{\citet{hartmann_flexible_2020}
priors}{@hartmann\_flexible\_2020 priors}}\label{hartmann_flexible_2020-priors}}

Table \ref{tab:hartmann-priors-data} contains the priors elicited by
\citet{hartmann_flexible_2020} for the parameters in the Preece-Baines
example. To generate the prior predictive samples displayed in Figure
\ref{fig:regression_prior_pred} we draw, for each user, \(\theta\) from
the corresponding lognormal distribution then compute \(h(t; \theta)\)
using Equation \eqref{eqn:preece-baines-model-definition-two} (without
the error term) and 250 values of \(t\) spaced evenly between ages \(2\)
and \(18\).

\input{tex-input/pbbo/preece-baines-growth/0031-hartmann-priors-data.tex}

\hypertarget{pareto-frontiers-for-the-covariate-independent-target}{%
\subsection{Pareto frontiers for the covariate-independent
target}\label{pareto-frontiers-for-the-covariate-independent-target}}

The Pareto frontier for the covariate-independent target and all values
of \(\kappa \in \mathcal{K}\) is displayed in Figure
\ref{fig:kappa_pop}.

\begin{figure}

{\centering \includegraphics{plots/pbbo/preece-baines-growth/pop-kappa.pdf} 

}

\caption{Pareto frontiers for each $\kappa \in \mathcal{K}$ for the \textbf{covariate-independent} example. The minimum loss point for each replicate is plotted with $\color{myredhighlight}{+}$. Note also that the loss scales differ between plots.}\label{fig:kappa_pop}
\end{figure}

\hypertarget{posterior-for-the-best-individual-under-the-flat-prior}{%
\subsection{\texorpdfstring{Posterior for the \emph{best} individual
under the flat
prior}{Posterior for the best individual under the flat prior}}\label{posterior-for-the-best-individual-under-the-flat-prior}}

Figure \ref{fig:regression_post_pred_best_indiv} differs only from
Figure \ref{fig:regression_post_pred} in that it displays the posterior
for the conditional mean for individual \(n = 1\), who has the minimum
number of diagnostic warnings under the flat prior.

\begin{figure}

{\centering \includegraphics{plots/pbbo/preece-baines-growth/regression-post-preds-best-indiv-under-flat.pdf} 

}

\caption{\textbf{Posterior} conditional mean $\pd(h(t; \theta) \mid Y_{n}, \lambda^{*})$ for individual $n = 1$, the individual with the fewest diagnostic warnings using the flat prior, with this individual's data displayed using crosses (red \textcolor{myredhighlight}{+}). Panels are otherwise identical to Figure \ref{fig:regression_prior_pred}, \textit{except} that all intervals are now 95\% wide (though many are too narrow to be visible).}\label{fig:regression_post_pred_best_indiv}
\end{figure}

\hypertarget{full-marginal-prior-and-posterior-comparison-plots}{%
\subsection{Full marginal prior and posterior comparison
plots}\label{full-marginal-prior-and-posterior-comparison-plots}}

Figures \ref{fig:pb_pop_prior_post_compare} and
\ref{fig:pb_cov_prior_post_compare} are extended versions of Figure
\ref{fig:small_cov_prior_post}, and display the prior and posterior
estimates for all the parameters in \(\theta\). Consistency and
uniqueness remain, evidently, challenging and as yet unobtainable.

\begin{landscape}
\begin{figure}

{\centering \includegraphics{plots/pbbo/preece-baines-growth/pop-priors-posteriors-compare.png} 

}

\caption{A comparison of the priors (\textcolor{mymidblue}{blue}) produced by our method using the covariate-independent marginal target (bottom two rows); and Hartmann et al. (2020) (second row), with no prior displayed for the flat prior scenario. The corresponding posteriors (\textcolor{myredhighlight}{red}) for individual $n = 26$ under each of these priors are displayed as dashed lines. Note that y-axes change within columns and are limited to values that clip some of the priors/posteriors for readability.}\label{fig:pb_pop_prior_post_compare}
\end{figure}

\begin{figure}

{\centering \includegraphics{plots/pbbo/preece-baines-growth/cov-priors-posteriors-compare.png} 

}

\caption{Otherwise identical to Figure \ref{fig:pb_pop_prior_post_compare} but the bottom two rows display the results obtained using the covariate-specific target.}\label{fig:pb_cov_prior_post_compare}
\end{figure}
\end{landscape}

\hypertarget{additional-information-for-the-r2-example}{%
\section{\texorpdfstring{Additional information for the \(R^{2}\)
example}{Additional information for the R\^{}\{2\} example}}\label{additional-information-for-the-r2-example}}

\hypertarget{a-comparison-to-an-asymptotic-result}{%
\subsection{A comparison to an asymptotic
result}\label{a-comparison-to-an-asymptotic-result}}

The poor fit for the Gaussian prior observed in Figure
\ref{fig:r2_roundtrip_full} could be attributed to issues in the
optimisation process, or to the lack of flexibility in the prior. To
investigate, we compare the results for \(\lambda_{\text{GA}}\) to
Theorem 5 of \citet{zhang_variable_2018}, which is an asymptotic result
regarding the optimal value of \(\lambda_{GA}\) for a target
\(\text{Beta}(s_{1}, s_{2})\) density for \(R^{2}\). We compare pairs of
\((n_{k}, p_{k})\) for \(k = 1, \ldots, 5\), noting that assumption (A4)
of Zhang and Bondell requires that \(p_{k} = \text{o}(n_{k})\) as
\(k \rightarrow \infty\) (for strictly increasing sequences \(p_{k}\)
and \(n_{k}\)). Thus we consider values of \(p\) such that
\(p_{1} = 80\) with \(p_{k} = 2p_{k - 1}\) and \(n\) with \(n_{1} = 50\)
and \(n_{k} = n_{k - 1}^{1.2}\), both for \(k = 2, \ldots, 5\). Each
\((n_{k}, p_{k})\) pair is replicated 20 times, and for each replicate
we generate a different \(\symbf{X}\) matrix with standard normal
entries. As the target density we choose \(s_{1} = 5, s_{2} = 10\) -- a
``more Gaussian'' target than previously considered and thus, we
speculate, possibly more amenable to translation with a Gaussian prior
for \(\beta\). We also use this example as an opportunity to assess if
there are notable differences between the Cramér-Von Mises discrepancy
and the Anderson-Darling discrepancy as defined in Equation
\eqref{eqn:discrepancies-definitions}. The support \(\Lambda\) for
\(\lambda_{\text{GA}}\) differs slightly from the example in the main
text, and is defined in Table \ref{tab:cap-lambda-def-asymp}, as
matching our target with larger design matrices requires considerably
larger values of \(\gamma\).

The computation of \(R^{2}\) becomes increasingly expensive as \(n_{k}\)
and \(p_{k}\) increase, which limits the value of some of our method's
tuning parameters. The approximate discrepancy function uses
\(S = 2000\) samples from the prior predictive and is evaluated using
\(I = 500\) importance samples. We run CRS2 for
\(N_{\text{CRS2}} = 500\) iterations, using \(N_{\text{design}} = 50\)
in the initial design for the subsequent single batch of Bayesian
optimisation, which uses \(N_{\text{BO}} = 100\) iterations.

\input{tex-input/pbbo/r2-examples/0015-cap-lambda-def-aysmp.tex}

\hypertarget{results-3}{%
\subsubsection{Results}\label{results-3}}

Figure \ref{fig:r2_asymp_plot} displays the results in terms of the
normalised difference between the \(\gamma\) we estimate
\(\gamma_{\text{pbbo}}^{*}\), and the asymptotic result of Zhang and
Bondell \(\gamma_{\text{asym}}^{*}\). Our typical finite sample estimate
is slightly larger than the asymptotic result, and the difference
increases with \(n_{k}\) and \(p_{k}\). The variability of the
normalised difference remains roughly constant, and thus reduces on an
absolute scale, though extrema seem to occur more frequently for larger
\(n_{k}\) and \(p_{k}\). These simulations suggest that the asymptotic
regime has not been reached even at the largest \(n_{k}\) and \(p_{k}\)
values we assessed.

\begin{figure}

{\centering \includegraphics{plots/pbbo/r2-examples/gamma-diff-plot.pdf} 

}

\caption{Relative difference between the value of $\gamma$ obtained using our methodology ($\gamma_{\text{pbbo}}^{*}$) and Theorem 5 of Zhang and Bondell (2018) ($\gamma_{\text{asym}}^{*}$).}\label{fig:r2_asymp_plot}
\end{figure}

The estimates of \(\gamma\) are not themselves particularly
illuminating: we should instead look for differences in the distribution
of \(R^{2}\) at the optima, which is to say on the ``data'' scale.
Figure \ref{fig:r2_target_vs_opt_prior} displays the target distribution
and the prior predictive distribution at the optima
\(\pd(R^{2} \mid \lambda^{*}_{GA})\). The fit is increasingly poor as
\(n\) and \(p\) increase, and there is little difference both between
the two discrepancies and with each discrepancies replications. The lack
of difference implies that the optimisation process is consistently
locating the same minima for \(D(\lambda)\). We conclude that either 1)
the ability of the model to match the target depends on there being
additional structure in \(\symbf{X}\), or 2) it is not possible to
encode the information in a \(\text{Beta}(5, 10)\) prior for \(R^{2}\)
into the Gaussian prior.

\begin{figure}

{\centering \includegraphics{plots/pbbo/r2-examples/optimal-pf-draws-plot.pdf} 

}

\caption{The target density $t(R^{2})$ and optimal prior predictive densities $\pd(R^{2} \mid \lambda^{*})$ under both the Cramér-von Mises (red, left column) and Anderson-Darling (blue, right column) discrepancies. There are 20 replicates of each discrepancy/target pair in this plot.}\label{fig:r2_target_vs_opt_prior}
\end{figure}

This example also further illustrates the difficulties inherent in
acquiring a prior for additive noise terms. Specifically, in this
example it is difficult to learn \((a_{1}, b_{1})\), despite the fact
that the contribution of \(\sigma^{2}\) to Equation
\eqref{eqn:r2-definition} is not purely additive. However, as we see in
Figure \ref{fig:r2_noise_hypers_plot}, estimates are uniformly
distributed across the permissible space, except for bunching at the
upper and lower bounds of \(\Lambda\). Note that for numerical and
computational stability, we constrain \(a_{1} \in (2, 50]\) and
\(b_{1} \in (0.2, 50]\) in this example. This contrasts with similarity
between replicates visible in Figure \ref{fig:r2_target_vs_opt_prior},
and is thus evidence that \((\hat{a}_{1}, \hat{b}_{1})\) have no
apparent effect on the value of \(D(\lambda^{*})\). We should instead
set the prior for \(\sigma^{2}\) based on external knowledge of the
measurement process for \(Y\).

\begin{figure}

{\centering \includegraphics{plots/pbbo/r2-examples/normal-noise-hyperpars-plot.pdf} 

}

\caption{Histograms of \textit{scaled} estimates of $(a_{1}^{*}, b_{1}^{*})$ for the settings considered in Section \ref{a-comparison-to-an-asymptotic-result}. Estimates have been scaled to $[0, 1]$ for visualisation purposes using the upper and lower limits defined in Table \ref{tab:cap-lambda-def}.}\label{fig:r2_noise_hypers_plot}
\end{figure}

The regularisation method we employ in the two other examples in the
main text is unlikely to assist in estimating \((a_{1}, b_{1})\).
Promoting a larger mean log marginal standard deviation, with the
knowledge \(D(\lambda)\) is insensitive to the value of
\((a_{1}, b_{1})\), would simply pick the largest possible value for
\(b_{1}^{2} \mathop{/} \left((a_{1} - 1)^{2}(a_{1} - 2)\right)\), which
occurs when \(a_{1}\) is at its minimum allowable value and \(b_{1}\)
its corresponding maximum.

\hypertarget{full-faithfulness-results}{%
\subsection{Full faithfulness
results}\label{full-faithfulness-results}}

The complete results from the faithfulness experiment are displayed in
Figure \ref{fig:r2_roundtrip_full_supp}.

\begin{figure}

{\centering \includegraphics{plots/pbbo/r2-examples/roundtrip-target-plot-big.pdf} 

}

\caption{As in Figure \ref{fig:r2_roundtrip_full} but for all values of $(s_{1}, s_{2})$ denoted in the facet panels titles. The performance of the regularised horseshoe is superior to the Dirichlet-Laplace, both of which are vast improvements over the Gaussian.}\label{fig:r2_roundtrip_full_supp}
\end{figure}