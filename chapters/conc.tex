\chapter{Conclusion}
\label{chp:conc}

The joint model for the observable quantities, and the latent parameters needed to describe them, is the starting point for Bayesian inference.
In isolation, our aim when specifying a joint model is to specify something just complex enough to describe the behaviour of the phenomena of interest at a given level of detail.
Such complexity should exist only because there are intricacies or interactions known to domain experts that materially affect the underlying phenomena, and/or because it is necessary to describe the observed data, including the way data collection occurs.
In practice, the complexity of the models we can specify is subject to a number of constraints, including the (lack of) information in both the data and available prior knowledge, the computational tools for approximating the posterior, and the credibility attributed to the model by decision makers who act on model output.

In this thesis we have introduced methods that enable the specification of sophisticated joint models that describe complex phenomena.
These methods do so by helping us incorporate multiple sources of information, including information from domain experts about the observable quantities.
The source-specific submodel approach which we adopted can assist in modelling the individual data sources more accurately, but with no additional complexity than is supported by the data.
Incorporating prior information about the observable quantities can greatly improve both the realism of our complex models by removing degenerate behaviour, and improve posterior computation.
Informative priors constructed using expert knowledge of the observable quantities have been instrumental in successfully approximating the posterior in many of the examples in this thesis.
Finally, by demonstrating that we have thoroughly interrogated each data source separately, and using a transparent process for incorporating elicited predictive information, we (hopefully) increase the credibility of our final joint model.

In Chapter \ref{chp:mphi} we extended Markov melding \citep{goudie_joining_2019} to the chained setting, where submodels share common quantities with adjacent submodels.
Of particular interest in this setting is understanding, and possibly preserving, the correlation between common quantities across submodels.
We paid considerable attention to the issue of pooling prior distributions where the constituent priors have only partly overlapping marginals, and proposed extensions to the standard linear and logarithmic pooling functions for this setting.
To approximate the chained melded posterior we proposed a variant of the multi-stage MCMC sampler, which we first discussed in Section \ref{sec:intro-multi-stage-sampler}, that partly parallelises the sampling process and makes it feasible\footnote{Suppose, hypothetically, we were able shoehorn into \texttt{Stan} the necessary spline basis functions and multiple root finder. It would then possible to write the chained melded model entirely in \texttt{Stan} and sample the chained melded posterior in a single step. It seems likely that this would be both extremely slow (as you have to automatically differentiate through the root finder) and result in Markov chains that mix poorly or otherwise misbehave.} to sample the posterior of this complex joint model.
We applied our methodology in two examples: an integrated population model where the implied joint model is clear and can be used as a reference for our results; and an investigation into respiratory failure where, prior to the melding process, the joint model for all observed data is not at all obvious.
The application to a survival analysis problem with uncertain event times is particularly interesting, as this has been discussed by a number of other authors \citep{wang_suicide_2020, oh_raking_2021, oh_considerations_2018, giganti_accounting_2020} and the chained melding model constitutes a conceptually appealing and pragmatic Bayesian approach to incorporating uncertainty, or measurement error, in a survival outcome.

The aforementioned multi-stage MCMC sampler has been a useful and pragmatic tool for posterior approximation throughout this thesis, and in Chapter \ref{chp:wsre} we ameliorated one numerical instability that arises when applying this sampler to the Markov melded model.
The particular numerical instability arose when an MCMC proposal was in a region of parameter space that was particularly \emph{im}probable under a specific $\pd_{\modelindex}(\phi)$, and this marginal was estimated via kernel density estimation.
To improve the accuracy of the estimate in this improbable region we applied the methodology for weighted samples, introduced in Section \ref{sec:length-biased-and-weighted-sampling}, to construct many weighted estimates of the prior marginal density, then combined them via a weighted average.
We applied our methodology in two examples of multi-stage samplers that target the Markov melded posterior which encounter the numerical stability, and showed that posterior samples produced using our methodology were plausible and not merely the result of numerical instability.
It is interesting that the first of these examples, the HIV example in Section \ref{sec:hiv-example-output}, is also considered in \citet{presanis_conflict_2013} as an example of a model where one of the data sources conflicts with the our available information.
The numerical instability we address is similar in spirit to the forms of conflict discussed by \citeauthor{presanis_conflict_2013} and others, but is specific to the multi-stage sampler and the target distributions in particular stages of the algorithm.

Lastly, in Chapter \ref{chp:pbbo} we considered the problem of translating elicited information that pertains to the observable quantity into an informative prior.
Performing this translation in a replicable manner is essential when crafting an appropriate informative prior that faithfully represents the expert's knowledge, and admits a posterior that is both amenable to posterior approximation and fits subsequently-collected data well.
Chapter \ref{chp:pbbo} approaches the problem of translation by defining a discrepancy between the prior predictive distribution, given a particular value of the hyperparameters $\lambda$, and uses a multi-stage global optimisation approach to minimise this discrepancy.
We demonstrated that often the model is sufficiently flexible to match the prior predictive distribution for multiple distinct values of the hyperparameters, and thus the optimisation problem is ill-posed.
We proposed a secondary objective to promote solutions that have wider marginal standard deviations and demonstrate that this has the intended effect on the prior, but it falls short of inducing uniqueness in some of the examples.

\section{Ideas for future research}

We now discuss some possible directions for future research, in addition to those discussed at the end of each chapter.

\subsection{Markov melding for submodels related in an arbitrary manner}

\input{tex-input/conc/0010-mphi-graph-of-models.tex}

The next conceptual hurdle for Markov melding is combining models with relationships more complex than that of the chain structure, for example the arbitrary ``graph of submodels'' displayed in Figure \ref{fig:mphi-graph-of-models}.
A Markov melding approach to this problem might proceed in the same manner as original melding and chained Markov melding: by marginalising specifying the joint model conditional on all the common quantities, and forming a common pooled prior for precisely these quantities.
Complications arise when the graph of models is not acyclical (i.e. loops in Figure \ref{fig:mphi-graph-of-models} would be conceptually challenging), so we assume that any cycles within the graph can be summarised in a single submodel.
This structure presents significant challenges to the multi-stage sampler we have previously employed.
As the number of submodels increases the order in which the multi-stage sampler operates will affect the quality of the posterior approximation, and the sampler must also consider the noninvertible relationships present in the graph (e.g.~in Figure~\ref{fig:mphi-graph-of-models} it must expand the target to include $\pd_{4}$ before $\pd_{3}$ and $\pd_{2}$ ahead of $\pd_{1}$).
However, research into multi-stage samplers for this type of melded model, and further extensions to Markov melding generally, must be motivated by specific applications requiring multiple sources of information and a sophisticated joint model.
%More applications of Markov melding would also surface opportunities for methodological and software improvement.

One barrier to the wider application of Markov melding is the amount of work/code needed to extract subposterior samples from one submodel for use as the proposal in the subsequent stage.
It would be easier to apply Markov melding if there existed software designed to take specific ``modules'' implementing a particular submodel and containing the corresponding data, and apply the multi-stage sampler (or other posterior approximation technique) to these abstract ``modules''.
But this is a difficult task, and to do so even for a small number of popular modelling frameworks is difficult\footnote{At least, it is in the experience of the author.}.
For each module we would need to be able to: evaluate the log-subposterior pointwise (not immediately possible for \texttt{BUGS}/\texttt{jags} models), sample the prior, reliably sample the subposterior, evaluate (analytically or approximately) the prior marginals for the common quantities, and record as ``metadata'' if the common quantity is the output of a noninvertible function and how to invert it if not.
Supposing we can construct abstract modules with these properties, it would then be easier to apply the multi-stage sampler in any order, check submodels for conflict\footnote{We could sample each module's subposterior, or the melded model excluding one module, and compare the subposteriors for the common quantity}, and develop efficient and generic sequential Monte Carlo algorithms for targeting the melded posterior.

\subsection{Further improving self-density ratio estimates using telescoping and path sampling}

One idea for further improving the weighted self-ratio estimate $\doublehat{\pdr}_{\text{WSRE}}(\phinu, \phide)$ is telescopic evaluation.
The problem is that if $\phinu$ and $\phide$ are very far away from each other (i.e.~more than two weighted target HDRs away), then at least one of the density estimates will be poor (in terms of the relative error) in the weighted sum.
Specifically, the idea is to find a series of $T$ \emph{telescoping points} $(\tilde{\phi}_{t})_{t = 1}^{T}$ that are somehow ``in between'' $\phinu$ and $\phide$ and evaluate $\doublehat{\pdr}_{\text{WSRE}}(\phinu, \phide)$ as
\input{tex-input/conc/0020-wsre-tele-def.tex}\noindent
Because each individual $\doublehat{\pdr}_{\text{WSRE}}$ is a consistent estimator for the self-density ratio, this is a consistent estimator for 
\input{tex-input/conc/0021-wsre-tele-theoretical.tex}\noindent
which makes clearer the telescoping sum and the cancellations that could theoretically occur.
For well chosen telescoping points, each individual ratio estimate will have minimal error associated with it and thus reduce the overall error in $\doublehat{\pdr}_{\text{WSRE}}^{\text{tele}}$ considerably.
The telescoping ratios, i.e. those not involving $\phinu$ or $\phide$ can also be cached for minimal additional computational cost.
Initial experiments in a trivial 1 dimensional problem suggested this approach did indeed reduce the error for far apart $\phinu$ and $\phide$, however in subsequent tests it did not materially affect our 1 dimensional example in Section \ref{sec:hiv-example-output}.
Choosing suitable telescoping points in 2 dimensions is significantly harder and more computationally intensive.
The similarity between Equation \eqref{eqn:wsre-tele-def} and the path sampling estimator of \citet{gelman_simulating_1998} suggests that adopting a path sampling approach may yield a more performant estimator in higher dimensional settings.

\subsection{Improving the computational and methodological components to better translate predictive information}

The methodology we propose in Chapter \ref{chp:pbbo} is by no means a complete solution to the
translation problem as we define it. As noted in
\citet{mikkola_prior_2021}, there are many avenues for further
methodological research and software development, which we now discuss.

\hypertarget{methodological}{%
\subsubsection{Methodological}\label{methodological}}
\addcontentsline{toc}{paragraph}{Methodological}

Perhaps the most important methodological issue remains the challenge of
inducing uniqueness from the under-specified optimisation problem. Given
our predictive CDF approach lacks uniqueness, we can infer that
approaches eliciting only quantiles (as many prior elicitation approaches do) also suffer from nonuniqueness, as a
CDF is equivalent to an infinite number of quantiles. Our attempt to
regularise the optimisation problem by simultaneously considering the
marginal variances for a model's parameters appears to work in the
Preece-Baines example, but is evidently fallible in the cure fraction
model. Alternatives to this form of regularisation, or additional terms
to be simultaneously optimised, may prove more reliable and performant.
For example, in certain mixture models it may be intuitive to consider
an additional term that promotes repulsion between components of
\(\theta\) \citep{quinlan_class_2021}. Specifically, assuming that
\(\lambda\) contains only the location parameters of the mixture
components, the following objective \citep[from][]{quinlan_class_2021}
promotes repulsion between the components when minimised
\input{tex-input/pbbo/0081-repulsiveness-objective.tex}\noindent where
\(c_{0} > 0\) is a constant controlling the ``repulsiveness'' of this
objective.

Another avenue to improve the identifiability and stability of the optimisation problem might be to borrow ideas from the literature on solving Fredholm integral equations of the first kind \citep{crucinio_solving_2022, gribok_backward_2004}.
Specifically, the secondary objective might instead compute the Kullback–Leibler divergence (or other appropriate divergence) between some chosen ``uninformative'' prior $\pd_{0}(\theta)$ and $\pd(\theta \mid \lambda)$.
How exactly one obtains $\pd_{0}(\theta)$ and its effect on the optimal prior are important questions to address for future work in this area.

The methodology we introduce makes no attempt to address uncertainty in
the elicited target. Incorporating this type of uncertainty seems
crucial given the known uncertainty in elicitation outputs
\citep{oakley_uncertainty_2007, dickey_beliefs_1980}, but would surely
make uniqueness an even more elusive property for prior translation
methods. A naive approach might consider a Bayesian model for \(\tc(Y)\)
given elicited information, then use draws from the posterior within
each optimisation iteration\footnote{A form of Markov melding.}.

Our methodology proceeds from a target predictive distribution, which
can be specified at \(R\) known covariate values. Precisely how one
selects \(R\) or the specific values of \(X_{r}\) at which to perform
predictive elicitation is important, but a full investigation is beyond
the scope of this chapter. In some studies the full design matrix
\(\symbf{X}\) is a prespecified experimental design, and we could
elicit targets corresponding to each row in \(\symbf{X}\). Such an
approach is unappealing as it requires considerable elicitation effort,
and if rows in \(\symbf{X}\) are similar then some targets will be
redundant. More generally, whilst larger values of \(R\) will yield a
more informative collection of targets, beyond a certain \(R\) the
information in an additional covariate-specific target is unlikely to
meaningfully affect the optimal prior distribution estimate. Larger
values of \(R\) also increase the possibility of eliciting marginally
``coherent'' \(\tc(Y \mid X_{r})\) yet ``incoherent'' joint
\(\tc(Y \mid \symbf{X})\). Instead, we could elicit targets at the
maximum feasible (given the amount of effort we are willing to expend)
number of design points that are somehow ``maximally spaced apart'', or
otherwise optimal under some criteria. Observational studies are subject
to the same constraints on \(R\), but -- at the point at which data are
available, yet before a first analysis is conducted -- we must choose a
subsample of the observed values of \(\symbf{X}\). Future
methodological developments that seek optimal values of \(X_{r}\) at
which to elicit information would be valuable \citep[possibly extensions
of the methods reviewed in][]{oleary_comparison_2009}. Optimal here
could be defined as the smallest possible value of \(R\) for inducing
uniqueness in the prior estimate (where possible).

\hypertarget{software}{%
\subsubsection{Software}\label{software}}
\addcontentsline{toc}{paragraph}{Software}

The \texttt{pbbo} package makes several trade-offs for applicability
that may be unnecessary with further software engineering efforts. We
make the critical choice to use gradient-free global optimisation
instead of automatic differentiation or Monte Carlo reparameterisation
gradient-based approach. This is done with our intended audience in
mind, which comprises statistical practitioners implementing models
directly in \texttt{R}. This setting, as of writing, lacks the
appropriate tooling\footnote{Here ``appropriate'' means akin to
  \texttt{JAX} \citep{bradbury_jax_2018} or like other
  \texttt{python}-native automatic differentiation libraries. An
  \texttt{R} equivalent could look like Radford Neal's \texttt{pqR}
  implementation of automatic differentiation, see
  \url{http://www.pqr-project.org/test/help-gradient.txt}.} necessary to
apply the aforementioned kinds of gradient-based methods. Our choice of
discrepancy also seems unsuited to gradient-based approaches as we must
differentiate, automatically or numerically, through both an importance
sampling estimate of an integral and a Monte Carlo ECDF. One notable
downside to the gradient-free approach is its computational cost
compared to gradient-based approaches, as the objective functions must
be evaluated many, many more times. Whilst similar in cost to other
prior specification methods \citep{wang_using_2018}, spending more time
estimating an appropriate prior distribution than sampling the
posterior, may prove unappealing and feels intuitively wrong.

We choose to depend on \texttt{mlrMBO} and \texttt{nloptr} for global
optimisation, as they contain mature and well-tested implementations of
multi-objective Bayesian optimisation via MSPOT, and CRS2. One downside
is that \texttt{mlrMBO} does not contain methods designed for noise in
only a subset of the objectives in a multi-objective setting. Our
current multi-objective approach thus ignores noise in
\(D(\lambda \mid \symbf{X})\). We attempt to ameliorate this by
minimising the noise via tuning parameters. Switching to noise-aware
method would likely improve multi-objective performance
\citep{daulton_parallel_2021}, with the experiment performed by
\citet{horn_first_2017} suggesting this is worthwhile for
heteroscedastic objectives. We also employ a single global optimisation
approach, but no single global optimisation algorithm is best for all
problems. Performance depends on the interaction between the
optimisation approach and the objectives, and thus future work on
\texttt{pbbo} in this direction would extend the variety of optimisers
available to users. Furthermore, many recent developments in global
optimisation
\citep{snoek_input_2014, salinas_quantile-based_2021, eriksson_scalable_2021, eriksson_scalable_2019}
lack implementations in \texttt{R}. More foundational software
development exposing these methods to \texttt{R} users would be
valuable. Improving the multi-stage optimisation process to be both
noise aware and have more choices of optimisers should enable us to
decrease the inter-replicate variability observed in the examples.
Finally, we have elected to develop our methodology in a stand-alone
\texttt{R} package instead of incorporating it into larger efforts for
statistical modelling
\citetext{\citealp[i.e.~\texttt{Stan}][]{stan_development_team_stan_2022}; \citealp[\texttt{brms}][]{burkner_brms_2017}}.
There is an irreconcilable tension between separate (and ideally
interoperable, see \citet{nicholson_interoperability_2022}) modules for
different stages of the Bayesian workflow \citep{gelman_bayesian_2020},
and the desire from practitioners for a single analysis environment. The
challenge remains to navigate this tension as the complexity of each
workflow step increases.
