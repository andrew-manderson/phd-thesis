---
title: Dani's corrections
date: Mon 19 Dec 12:04:09 2022
---

- [x] Page 31, line -13 (13 from the bottom): “objective” repeated twice
- [x] Page 101, line -6  : plausible
- [x] Page 130, line -3: did you mean (right panel) rather than (left panel)? (AM: and there was a typo in the figure caption)
- [ ] Page 133, Figure 4.6: labels on the right hand side. Did you mean t=2, t=8 etc or X=2, X=8 etc?
    -> Requires regenerating the plot and changing the labels to X_{1} = 2, etc. This will need to be done on the turing laptop because doing so here is quite annoying
- [x] Page 147, caption to figure 4.12: “the” repeated twice
