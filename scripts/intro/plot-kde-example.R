library(tibble)

source("scripts/common/setup-ggplot-theme.R")
source("scripts/common/setup-argparse.R")

args <- parser$parse_args()

set.seed(2349817)

n_samples <- 60
x_samples <- c(
  rgamma(n = 0.8 * n_samples, shape = 3, rate = 1.5),
  rgamma(n = 0.2 * n_samples, shape = 50, rate = 10)
)

eval_points <- seq(from = -0.2, to = 8, length.out = 500)
const_bw <- bw.SJ(x_samples)
print(sprintf("const_bw = %.3f", const_bw))

f_true <- function(x) {
  0.8 * dgamma(x, shape = 3, rate = 1.5) +
    0.2 * dgamma(x, shape = 50, rate = 10)
}

compute_zvals <- function(x, bw) {
  sapply(X = x, function(z) (z - x_samples) / bw)
}

kde_rect <- function(x, bw = const_bw) {
  z_vals <- compute_zvals(x, bw)
  inds <- abs(z_vals) < 1
  res <- (1 / 2) * apply(inds, 2, sum)
  res / (bw * n_samples)
}

kde_gauss <- function(x, bw = const_bw) {
  z_vals <- compute_zvals(x, bw)
  kernel_vals <- dnorm(z_vals)
  res <- apply(kernel_vals, 2, sum)
  res / (bw * n_samples)
}

kde_cosine <- function(x, bw = const_bw) {
  z_vals <- compute_zvals(x, bw)
  inds <- abs(z_vals) < 1
  kernel_vals <- (pi / 4) * cos((pi / 2) * z_vals)
  kernel_vals[!inds] <- 0
  apply(kernel_vals, 2, sum) / (bw * n_samples)
}

plot_tbl <- tibble(
  x = rep(eval_points, times = 3),
  y = c(
    kde_rect(eval_points),
    kde_gauss(eval_points),
    kde_cosine(eval_points)
  ),
  grp = rep(
    c("Rectangular", "Gaussian", "Cosine"),
    each = length(eval_points)
  ) %>%
    factor(x = ., levels = c("Cosine", "Gaussian", "Rectangular"))
)

p1 <- ggplot(plot_tbl) +
  geom_line(aes(x = x, y = y, linetype = grp, colour = grp), alpha = 0.8) +
  geom_rug(
    data = tibble(x = x_samples),
    mapping = aes(x = x),
    inherit.aes = FALSE
  ) +
  stat_function(fun = f_true, n = 2e3) +
  xlab(expression(italic(x))) +
  ylab("Density") +
  scale_color_manual(
    name = "Kernel: ",
    values = c(
      "Rectangular" = blues[2],
      "Gaussian" = greens[2],
      "Cosine" = highlight_col
    )
  ) +
  scale_linetype_manual(
    name = "Kernel: ",
    values = c(
      "Rectangular" = "3131",
      "Gaussian" = "solid",
      "Cosine" = "5111"
    )
  ) +
  theme(legend.position = "bottom") +
  guides(
    colour = guide_legend(title = "Kernel: ", reverse = TRUE),
    linetype = guide_legend(title = "Kernel: ", reverse = TRUE)
  )

ggsave_fullpage(
  filename = args$output,
  plot = p1,
  adjust_height = - (7 / 12) * display_settings$full_page_plot_height
)
